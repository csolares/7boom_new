module Sass::Script::Functions

  Sass::Script::Number.precision = 10

  module MostroExtensions

    @@fontSize = Sass::Script::Number.new(16.0)

    def setFontSize(number)
      @@fontSize = number
    end

    def bodyFontSize(number = nil)
      setFontSize(number) if !number.nil?
      Sass::Script::String.new("%spx" % @@fontSize)
    end

    def pem(px)
      Sass::Script::String.new( "%gem" % [px.value / @@fontSize.assert_int!] )
    end

  end

  include MostroExtensions

end
