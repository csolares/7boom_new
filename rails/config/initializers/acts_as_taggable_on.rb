ActsAsTaggableOn::Tag.class_eval do
  acts_as_url :name,
    :url_attribute => :slug,
    :sync_url => true
end
