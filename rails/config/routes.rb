SevenBoom::Application.routes.draw do

  ActiveAdmin.routes(self)

  devise_for :admin_users, ActiveAdmin::Devise.config

  root :to => 'booms#home'

  devise_for  :users,
              :path_names =>  {
                :sign_in => "login",
                :sign_out => "logout",
                :sign_up => "register"
              },
              :controllers => {
                :omniauth_callbacks => "omniauth_callbacks",
                :registrations      => "users/registrations"
              }

  # boom operations
  match "/boom/nuevo"                 => "booms#edit",        :as => "new_boom"
  match "/:id/edit"                   => "booms#edit",        :as => "edit_boom"
  match "/boom/fav"                   => "booms#fav",         :as => "boom_fav"
  match "/boom/unfav"                 => "booms#unfav",       :as => "boom_unfav"
  match "/boom/search"                => "booms#search",      :as => "boom_search", :via => "post"

  # user stuff
  match "/usuario/:id"                => "users#profile"
  match "/usuario/:id/:username"      => "users#profile",     :as => "user_profile"

  # boom views
  match "/:category_slug/:boom_slug"  => "booms#show",        :as => "boom"
  match "/:category_slug"             => "booms#bycategory",  :as => "bycategory"

end
