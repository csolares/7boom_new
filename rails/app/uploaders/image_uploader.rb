# encoding: utf-8

class ImageUploader < CarrierWave::Uploader::Base

  include CarrierWave::MiniMagick

  storage :file

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  version :large do
    process :resize_to_fill => [685, 382]
  end

  version :boom do
    process :resize_to_fill => [587, 387]
  end

  version :wide_thumb do
    process :resize_to_fill => [158, 90]
  end

  version :thumb do
    process :resize_to_fill => [133, 75]
  end

end
