# encoding: utf-8

class AvatarUploader < CarrierWave::Uploader::Base

  include CarrierWave::MiniMagick

  storage :file

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  process :quality => 100
  process :resize_to_fill => [145,145]

  version :medium_thumb do
    process :resize_to_fill => [85, 85]
  end

  version :small_thumb do
    process :resize_to_fill => [55, 55]
  end

end
