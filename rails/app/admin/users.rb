ActiveAdmin.register User do

  index do
    column "ID",        :id
    column "username",  :username
    column              :email
    column "Nombre",    :first_name
    column "Apellidos", :last_name
    column              :avatar do |user|
      link_to(
        image_tag(user.avatar.small_thumb.url),
        user.avatar.url,
        :target => :_blank
      )
    end
    column "¿Es colaborador?", :sortable => :collaborator do |user|
      user.collaborator ? "✓" : ""
    end
    default_actions
  end

  form :multiplart => true do |f|
    f.inputs "User" do
      f.input :username
      f.input :first_name
      f.input :last_name
      f.input :avatar,
              :hint => f.object[:avatar] ? f.template.image_tag("/uploads/user/avatar/%s/small_thumb_%s" % [f.object[:id], f.object[:avatar]]) : ""
      f.input :bio
      f.input :collaborator
      f.input :email
      f.input :twitter_url
      f.input :facebook_url
      f.input :password
    end
    f.actions

  end


end
