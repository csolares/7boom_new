# encoding: utf-8
ActiveAdmin.register Category do

  menu :label => "Categorías"

  config.sort_order = "position_asc"

  index :title => 'Categorías' do
    column :id, :sortable => false
    column "Nombre", :name, :sortable => false
    default_actions
  end

  form do |f|
    f.inputs "Categoría" do
      f.input :name,      :label => 'Nombre'
      f.input :position,  :label => 'Posición'
      f.input :featured,  :label => '¿Es destacada?'
    end
    f.actions
  end

end
