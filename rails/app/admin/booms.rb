# encoding: utf-8
ActiveAdmin.register ActsAsTaggableOn::Boom, :as => "Boom" do

  before_filter do
    # previene que las urls a los booms
    # se formen con slug, en el admin
    # las necesitamos con id
    Boom.class_eval do
      def to_param
        id.to_s
      end
    end
  end

  index do

    column "Título",    :title
    column "Categoría", :category
    column "Autor",     :user
    column "Recomendado" do |boom|
      if !boom.featured.nil?
        raw("Promovido el %s %s" % [boom.featured.to_formatted_s(:short), link_to("[quitar]", "#todo")])
      else
        link_to("Recomendar", "#todo")
      end
    end
    default_actions
  end

  form :partial => "form"

  controller do
    def search_booms
      @booms = Boom.find_by_query params[:query]
      redirect_to :back
    end
  end

end
