(function ($) {
  "use strict";

  function boomSearch() {
    "use strict";
    var searchbox = $("#boom_boom_title"),
        form = searchbox.closest("form"),
        fields = form.find(":hidden"),
        cache  = {},
        processItem = function (item) {
          var image = typeof (item.image) === "object" ? "<img width='34' height='34' src='" + item.image[0]["#text"] + "'> " : "";
          return {
            label: item.boom.title,
              value: item.boom.title,
              title: item.boom.title,
          };
        };

    searchbox
      .autocomplete({
        autoFocus: false,
        html: true,
        minLength: 3,
        source: function (request, response) {

          var term = $.trim(request.term.toLowerCase());

          if (cache[term]) {
            response(cache[term]);
            return;
          }

          $
        .ajax({
          url: "/boom/search",
          data: {
            method: "search",
          param: term,
          format: 'json',
          limit: 10
          },
          type: 'GET',
          dataType: 'json'
        })
      .done(function (data) {
        if (typeof (data.results.boomMatches) === "object") {
          var mapped = [];
          if (typeof (data.results.boomMatches.boom.title) === "string") {
            mapped = [processItem(data.results.boomMatches.title)];
          } else {
            mapped = $.map(data.results.boomMatches.title, function (item) {
              return processItem(item);
            });
          }
          cache[term] = mapped;
          response(mapped);
        }
      })
      .always(function () {
        searchbox.removeClass("ui-autocomplete-loading");
      });

        },
        select: function (event, ui) {
          fields.each(function (i, elem) {
            elem.value = ui.item[elem.id] && ui.item[elem.id] || elem.value;
          });
        }
      });

  }

  boomSearch();

}(jQuery));
