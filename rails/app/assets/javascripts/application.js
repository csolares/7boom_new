//= require jquery.min
//= require bootstrap
var collapser = {
  main : function(trigger, target){
    trigger = $(trigger);
    target = $(target);
    trigger.click(function(){
      if (trigger.hasClass('active')){
        target.slideUp();
        trigger.removeClass('active');
      }else {
        target.slideDown();
        trigger.addClass('active');
      }
      
    });
  }
};

var formtasty = {
  main: function () {
    var item;
    var taste;
    item = $(".formtasty");    
    item.each(function () {
      
      item = $(this)
      taste = item.prop('tagName').toLowerCase();
      if(taste == "select"){
      
        formtasty.select(item);
        
      }else{
      
        taste = item.attr('type');
        switch(taste){
          case 'radio':
            
          break;
          case 'checkbox':
            
          break;
          case 'file':
            
          break;
        }
        
      }
      
    });
    
    
        
  }, 
  select: function(item){
    
  },
  radio: function (item) {
    
  },
  checkbox: function (item) {
    
  },
  file: function (item) {
    
  }
};


(function ($) {
  "use strict";

  //Panel Toggle
  $('#panel-switch').click(function () {
    var userPanel = $('#user-panel');
    if (userPanel.hasClass('is-closed')) {
      userPanel.find('.panel').slideDown(300, function () {
        userPanel.removeClass('is-closed').addClass('is-active');
      });
      $(this).find('a').text('Ocultar');
    } else {
      userPanel.find('.panel').slideUp(300);
      userPanel.removeClass('is-active').addClass('is-closed');
      $(this).find('a').text('Tu Panel');
    }
  });

  //TODO - Center marker
  /*$.centerMark = function() {
    var marker = $('.marker'),
        centerTo = marker.parents('.has-marker').find('.is-active'),
        center = centerTo.offset.left + ( centerTo.outerWidth / 2);
    console.log( centerTo );
    console.log( center );

  }

  $.centerMark();*/
  collapser.main('#panel-trigger', '#panel-wrap');
  formtasty.main();
}(jQuery));

//Bootstrap enabled tabs
$('.tabs a').click(function (e) {
  e.preventDefault();
  $(this).tab('show');
})

