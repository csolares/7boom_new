class Element < ActiveRecord::Base

  attr_accessible :community_position, :content, :position, :title, :image_attributes

  belongs_to :boom
  belongs_to :image
  accepts_nested_attributes_for :image

  validates :title,
            :presence => {
              :message => 'Te falta este título'
            }

end
