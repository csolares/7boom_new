class Image < ActiveRecord::Base

  attr_accessible :path

  belongs_to :user

  mount_uploader :path, ImageUploader

  validates :path, :presence => true

  def recreate_image
    if image?
      image.recreate_versions!
    end
  end

end
