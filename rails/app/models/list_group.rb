class ListGroup < ActiveRecord::Base

  # Este modelo sirve para agrupar los tops de cada categoría y
  # el slider del home

  attr_accessible :block, :name, :position, :slug, :list_element_attributes

  has_many :list_element

  accepts_nested_attributes_for :list_element

  acts_as_url :name,
    :url_attribute => :slug,
    :sync_url => true

end
