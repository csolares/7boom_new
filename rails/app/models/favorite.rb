class Favorite < ActiveRecord::Base

  belongs_to :boom
  belongs_to :user

  attr_accessible :boom_id, :user_id

  after_create :activity

  def activity

    # Se agrega el favorito a la
    # actividad del usuario, solo si
    # no la había agregado previamente

     activity = Activity.find_or_initialize_by_boom_id_and_user_id_and_data(
      :boom_id => self.boom_id,
      :user_id => self.user_id,
      :data    => 'fav'
    )

    activity.save if activity.new_record?

  end

end
