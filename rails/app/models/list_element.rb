class ListElement < ActiveRecord::Base
  belongs_to :image
  belongs_to :boom
  belongs_to :list_group

  attr_accessible :position, :summary, :title, :url, :image_attributes, :image_id, :boom_id, :boom_attributes

  accepts_nested_attributes_for :image, :boom

  validates :title,   :presence   => {
    :message => 'Te falta el título del boom'
  }
  validates :image,   :presence   => {
    :message => 'Te falta agregar la imagen'
  }
  validates :summary,   :presence   => {
    :message => 'Te falta el resumen'
  }
  validates :url,   :presence   => {
    :message => '¿A qué URL debe llevar el boom?'
  }

end
