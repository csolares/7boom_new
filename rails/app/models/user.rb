class User < ActiveRecord::Base

  attr_accessor :login

  attr_accessible :avatar, :bio, :collaborator, :first_name, :last_name, :username, :social_visible,
                  :email, :password, :password_confirmation, :remember_me, :access_token, :avatar_cache, :remove_avatar,
                  :facebook_url, :twitter_url, :provider, :login

  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,:omniauthable,
         :recoverable, :rememberable, :trackable, :validatable,
         :authentication_keys => [:login]

  has_many :boom,     :dependent => :delete_all
  has_many :favorite, :dependent => :delete_all

  validates_presence_of :username
  validates_uniqueness_of :username

  mount_uploader :avatar, AvatarUploader

  def recreate_avatar
    if avatar?
      avatar.recreate_versions!
    end
  end

  def self.from_omniauth(data)

    attributes = {}

    user = where(data.slice(:provider, :uid)).first_or_create do |user|
      user.uid                = data.uid
      user.access_token       = data.credentials.token
      user.username           = data.info.nickname

      if data.provider == 'facebook'
        user.email = data.info.email
        user.remote_avatar_url  = data.info.image.gsub("square", "large")
      else
        user.bio = data.info.description
        user.remote_avatar_url = data.info.image.gsub("_normal", "")
      end
    end

    if data.provider == 'facebook'
      attributes = {
        :first_name    => data.first_name,
        :last_name     => data.last_name,
        :facebook_url  => data.extra.raw_info.link
      }
    else
      attributes = {
        :twitter_url  => data.info.urls.Twitter
      }
    end

    user.update_attributes(attributes)
    user

  end

  def self.new_with_session(params, session)
    if session["devise.user_attributes"]
      new(session["devise.user_attributes"], without_protection: true) do |user|
        user.attributes = params
        user.valid?
      end
    else
      super
    end
  end

  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    else
      where(conditions).first
    end
  end

  def email_required?
    super && provider.blank?
  end

  def update_with_password(params, *options)
    if encrypted_password.blank?
      update_attributes(params, *options)
    else
      super
    end
  end

end
