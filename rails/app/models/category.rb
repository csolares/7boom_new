# encoding: utf-8
class Category < ActiveRecord::Base
  attr_accessible :featured, :name, :position, :slug

  acts_as_url :name,
    :url_attribute => :slug,
    :sync_url => true

end
