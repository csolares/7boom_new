# encoding: utf-8
class Boom < ActiveRecord::Base

  attr_accessible :nsfw, :parent_id, :slug, :summary, :title, :user_id, :category_id,
                  :element_attributes, :image_attributes, :tag_list, :featured

  just_define_datetime_picker :featured, :add_to_attr_accessible => true

  belongs_to  :user
  belongs_to  :category
  belongs_to  :image
  has_many    :element,
              :order => 'position ASC, id ASC',
              :dependent => :delete_all

  accepts_nested_attributes_for :image, :category
  accepts_nested_attributes_for :element,
                                :reject_if => :all_blank,
                                :allow_destroy => true

  acts_as_taggable
  acts_as_nested_set

  acts_as_url :title,
    :url_attribute => :slug,
    :sync_url => true

  validates :title,   :presence   => {
    :message => 'Te falta el título del boom'
  }
  validates :slug,    :uniqueness => true
  validates :summary, :presence   => {
    :message => 'Falta la descripción del boom'
  }
  validates :user_id, :presence   => true

  validates :element,
    :length => {
      :minimum => 7,
      :maximum => 7,
      :message => "Oh, you! Debes escribir 7 puntos, no más, no menos"
    }

  #pretty urls
  def to_param
    "#{category.slug}/#{slug}"
  end

  def recreate_image
    if image?
      image.recreate_versions!
    end
  end

end
