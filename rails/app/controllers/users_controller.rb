class UsersController < ApplicationController

  def profile
    @user = User.find_by_id(params[:id])

    if params[:username].nil? or @user.username != params[:username]
      redirect_to user_profile_path(@user.id, @user.username)
    end

    @booms =  Boom
                .where({ :user_id => @user.id})
                .order("created_at DESC")
                .page(params[:page])
                .per(10)

  end

  def collaborators
  end

end
