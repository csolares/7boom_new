class OmniauthCallbacksController < Devise::OmniauthCallbacksController

  def all

    #raise request.env["omniauth.auth"].to_yaml

    data = request.env["omniauth.auth"]
    user = User.from_omniauth(data)

    if user.persisted?
      flash.notice = "¡Hola!"
      sign_in_and_redirect user
    else
      session["devise.user_attributes"] = user.attributes
      redirect_to new_user_registration_url
    end

  end

  alias_method :twitter,  :all
  alias_method :facebook, :all
end
