class Users::RegistrationsController < Devise::RegistrationsController

  def new
    cleaner
    super
  end

  def create
    cleaner
    super
  end

  def edit
    super
  end

  def update
    cleaner

    @user = User.find(current_user.id)
    email_changed = @user.email != params[:user][:email]
    is_omniauth_account = !@user.provider.blank?

    successfully_updated = if !is_omniauth_account
      @user.update_with_password(params[:user])
    else
      @user.update_without_password(params[:user])
    end

    if successfully_updated
      # Sign in the user bypassing validation in case his password changed
      sign_in @user, :bypass => true
      redirect_to user_profile_path(@user.id, @user.username)
    else
      render "edit"
    end

  end

  # previene la inyección de campos que
  # no son pueden ser administrados por
  # su propietario
  private
  def cleaner
    [
      :collaborator, :reset_password_token, :reset_password_sent_at, :remember_created_at,
      :sign_in_count, :current_sign_in_at, :last_sign_in_at, :current_sign_in_ip,
      :last_sign_in_ip, :uid, :provider, :access_token, :created_at, :updated_at
    ].each do |field|
      params[:user].delete field
    end
  end

end
