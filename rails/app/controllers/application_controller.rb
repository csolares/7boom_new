class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :add_devise_views

  def add_devise_views
    prepend_view_path "#{Rails.root}/app/views/devise"
  end

end
