class BoomsController < ApplicationController

  def home
    # slider
    @slider = nil

    # recomendados
    @featured = Boom.order("featured DESC").limit(7)

    @weekly = ListGroup.find_by_block_and_name( "home_page", "semanal" )

  end

  def show
    @boom = Boom.find_by_slug!(params[:boom_slug])
    if current_user
      @fav = Favorite.find_by_user_id_and_boom_id(
        current_user.id,
        @boom.id
      ) || Favorite.new
    end
  end

  def bycategory
    @booms = Boom.joins(:category).where('categories.slug' => params[:category_slug])
  end

  def edit
    redirect_to user_session_path if !current_user

    @boom = Boom.new

    if params[:id]

      # edit GET
      @boom = Boom.find_by_id!(params[:id])

      # check authorship
      if !current_user.id.equal?(@boom.user_id)
        flash[:notice] = "Oh, you! ese no es un boom tuyo"
        redirect_to root_path
      end

    end

    if params[:boom]
      #edit POST
      @boom.user_id = current_user.id

      # el slice previene inyección de campos adicionales que pudieran
      # ser importantes, como :featured que debe ser decidido por un
      # administrador, no por el autor

      sanitized_params = params[:boom].slice(:title, :summary, :category_id, :image_attributes, :tag_list, :element_attributes)

      #params[:boom][:element_attributes].each do |i, element|
      #  params[:boom][:element_attributes][i].delete :community_position if params[:element][i][:community_position]
      #end

      #sanitized_params[:element_attributes] = params[:element_attributes]

      if @boom.update_attributes sanitized_params
        redirect_to boom_path(@boom.category.slug, @boom.slug)
      end
      # otherwise let the form render,
      # the errors will be displayed with
      # f.object.errors
    elsif !params[:id]
      #new GET
      7.times { @boom.element.build }
    end

    @i = 0

  end

  # adds a favorite
  def fav
    response = {
      :message => "Tienes que registrarte para tener favoritos"
    }
    if current_user and params[:favorite][:boom_id]
      fav = Favorite.find_or_initialize_by_boom_id_and_user_id(
        params[:favorite][:boom_id],
        current_user.id
      )
      if fav.new_record?
        if fav.save
          response[:message] = "Hecho"
        else
          response[:error] = fav.errors.full_messages.last
        end
      end
    end
    if request.xhr?
      render :json => response
    else
      redirect_to :back
    end
  end

  # removes a favorite
  def unfav
    response = {
      :message => "Tienes que registrarte modificar favoritos"
    }
    if current_user and params[:favorite][:boom_id]
      fav = Favorite.find_by_boom_id_and_user_id!(
        params[:favorite][:boom_id],
        current_user.id
      )
      if fav.destroy
        response[:message] = "Hecho"
      else
        response[:error] = fav.errors.full_messages.last
      end
    end
    if request.xhr?
      render :json => response
    else
      redirect_to :back
    end
  end

  def search
    if request.xhr?
      response = {
        :message => message,
      }
      render :json => response.to_json
    else
      flash[:notice] = message
      redirect_to :back
    end
  end

end
