class ChangeImageFromBoom < ActiveRecord::Migration
  def change
    remove_column :booms, :image
    add_column    :booms, :image_id, :integer, :after => :slug
    add_index     :booms, :image_id
  end
end
