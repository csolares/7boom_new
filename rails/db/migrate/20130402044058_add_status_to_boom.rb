class AddStatusToBoom < ActiveRecord::Migration
  def change
    add_column :booms, :status, :integer, :after => :depth, :default => 2
  end
end
