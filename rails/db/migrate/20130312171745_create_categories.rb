class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :slug
      t.string :name
      t.integer :position
      t.boolean :featured

      t.timestamps
    end
  end
end
