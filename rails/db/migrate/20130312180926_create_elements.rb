class CreateElements < ActiveRecord::Migration
  def change
    create_table :elements do |t|
      t.references :boom
      t.string :title
      t.references :image
      t.text :content
      t.integer :position
      t.integer :community_position

      t.timestamps
    end
    add_index :elements, :boom_id
    add_index :elements, :image_id
  end
end
