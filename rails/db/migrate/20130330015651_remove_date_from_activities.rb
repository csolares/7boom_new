class RemoveDateFromActivities < ActiveRecord::Migration
  def up
    remove_column :activities, :date
  end

  def down
    add_column :activities, :date, :datetime
  end
end
