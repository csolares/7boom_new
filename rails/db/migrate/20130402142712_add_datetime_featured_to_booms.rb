class AddDatetimeFeaturedToBooms < ActiveRecord::Migration
  def change
    add_column :booms, :featured, :datetime, :after => :depth
  end
end
