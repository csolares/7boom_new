class AddFeaturedToBooms < ActiveRecord::Migration
  def change
    add_column :booms, :featured, :boolean
  end
end
