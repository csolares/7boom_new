class CreateBooms < ActiveRecord::Migration
  def change
    create_table :booms do |t|
      t.references :user
      t.string :title
      t.string :slug
      t.string :image
      t.string :summary
      t.boolean :nsfw
      t.references :category
      t.integer :parent_id
      t.integer :lft
      t.integer :rgt
      t.integer :depth

      t.timestamps
    end
    add_index :booms, :user_id
    add_index :booms, :category_id
  end
end
