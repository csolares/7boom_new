class CreateListGroups < ActiveRecord::Migration
  def change
    create_table :list_groups do |t|
      t.string :block
      t.integer :position
      t.string :name
      t.string :slug

      t.timestamps
    end
  end
end
