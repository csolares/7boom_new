class AddSocialNetworkDataToUser < ActiveRecord::Migration
  def change
    add_column :users, :facebook_url, :string, :after => :access_token
    add_column :users, :twitter_url, :string, :after => :facebook_url
  end
end
