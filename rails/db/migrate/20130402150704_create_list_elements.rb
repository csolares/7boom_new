class CreateListElements < ActiveRecord::Migration
  def change
    create_table :list_elements do |t|
      t.references :image
      t.references :boom
      t.references :category
      t.references :list_group
      t.integer :position
      t.string :title
      t.text :summary
      t.string :url

      t.timestamps
    end
    add_index :list_elements, :image_id
    add_index :list_elements, :boom_id
    add_index :list_elements, :category_id
    add_index :list_elements, :list_group_id
  end
end
