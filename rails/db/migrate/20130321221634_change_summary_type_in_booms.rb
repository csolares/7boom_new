class ChangeSummaryTypeInBooms < ActiveRecord::Migration
  def change
    change_table :booms do |b|
      b.change :summary, :text, :null => false
    end
  end
end
