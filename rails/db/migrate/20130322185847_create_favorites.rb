class CreateFavorites < ActiveRecord::Migration
  def change
    create_table :favorites do |t|
      t.references :boom
      t.references :user

      t.timestamps
    end
    add_index :favorites, :boom_id
    add_index :favorites, :user_id
  end
end
