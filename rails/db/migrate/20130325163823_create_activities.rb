class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.references :boom
      t.references :user
      t.datetime :date
      t.text :data

      t.timestamps
    end
    add_index :activities, :boom_id
    add_index :activities, :user_id
  end
end
