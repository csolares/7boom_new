class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|

      ## Profile
      t.string :username
      t.string :first_name
      t.string :last_name
      t.string :avatar
      t.text :bio
      t.boolean :collaborator, :null => false, :default => false
      t.boolean :social_visible

      ## Database authenticatable
      t.string :email,              :null => false, :default => ""
      t.string :encrypted_password, :null => false, :default => ""

      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at

      ## Trackable
      t.integer  :sign_in_count, :default => 0
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip

      # Social login
      t.string :uid
      t.string :provider
      t.string :access_token

      t.timestamps
    end

    add_index :users, :email,                 :unique => true
    add_index :users, :reset_password_token,  :unique => true

  end
end
