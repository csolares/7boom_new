class RemoveFeaturedFromBooms < ActiveRecord::Migration
  def up
    remove_column :booms, :featured
  end

  def down
    add_column :booms, :featured, :boolean
  end
end
