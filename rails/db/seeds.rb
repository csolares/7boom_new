# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


list_group = [
  { block: "home_page", name: "top", position: 0, slug: "top"},
  { block: "musica", name: "top", position: 0, slug: "top-1"},
  { block: "cine", name: "top", position: 0, slug: "top-2"},
  { block: "deportes", name: "top", position: 0, slug: "top-3"},
  { block: "home_page", name: "semanal", position: 1, slug: "semanal"},
  { block: "tech-vg", name: "top", position: 0, slug: "top-4"},
  { block: "humor", name: "top", position: 0, slug: "top-5"},
  { block: "sexo", name: "top", position: 0, slug: "top-6"},
  { block: "cine-tv", name: "top", position: 0, slug: "top-7"},
  { block: "comida", name: "top", position: 0, slug: "top-8"},
  { block: "geek", name: "top", position: 0, slug: "top-9"},
  { block: "ocio", name: "top", position: 0, slug: "top-10"}
]

lists = ListGroup.create( list_group )
